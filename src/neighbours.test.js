import { neighbours } from './neighbours.js'

function gen(x, y) {
    let array = []
    let i = 0
    for (y -= 1; y >= 0; y--) {
        let row = []
        for (let col = 0; col < x; col++) {
            row.push(i++)
        }
        array.push(row)
    }
    return (x, y) => array[y][x]
}

test('Happy path, a 3 * 3 array and checking the middle', () => {
    expect(neighbours(gen(3, 3), 1, 1, 3, 3)).toEqual([0, 1, 2, 3, 5, 6, 7, 8])
})

test('Neighbours from 5 * 5 array', () => {
    expect(neighbours(gen(5, 5), 1, 1, 5, 5)).toEqual([
        0,
        1,
        2,
        5,
        7,
        10,
        11,
        12,
    ])
})

test('Neighbours wrapping round bottom in a 5 * 5 array', () => {
    expect(neighbours(gen(5, 5), 1, 4, 5, 5)).toEqual([
        15,
        16,
        17,
        20,
        22,
        0,
        1,
        2,
    ])
})

test('Neighbours wrapping round left in a 5 * 5 array', () => {
    expect(neighbours(gen(5, 5), 0, 1, 5, 5)).toEqual([
        4,
        0,
        1,
        9,
        6,
        14,
        10,
        11,
    ])
})

test('Neighbours wrapping from top right corner of 5 * 5 array', () => {
    expect(neighbours(gen(5, 5), 4, 0, 5, 5)).toEqual([
        23,
        24,
        20,
        3,
        0,
        8,
        9,
        5,
    ])
})

test('Throws if neighbours are less than 3 * 3 array', () => {
    expect(() => neighbours(gen(1, 1), 0, 0, 1, 1)).toThrow(RangeError)
    expect(() => neighbours(gen(3, 2), 0, 0, 3, 2)).toThrow(RangeError)
    expect(() => neighbours(gen(2, 3), 0, 0, 2, 3)).toThrow(RangeError)
    expect(() => neighbours(gen(2, 3), 0, 0, 2, 3)).toThrow(
        'To find all neighbours, the array must be of at least size 3 * 3'
    )
})
