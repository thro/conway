# Convway's game of life

This repository contains a version of [Convway's game of life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) made using the [Svelte](https://svelte.dev/) web application framework.

## The project

This was a weekend hobby to explore the Svelte framework a bit deeper than just reading about it. And I must say that I enjoyed working with Svelte allot, plus I got to refresh and strengthen my JavaScript and CSS skills.

You can find a working version on the web [https://conway.iox.is/](https://conway.iox.is/)

And here is a screen-shot

![Screenshot of the game](screenshot.png)

## Build

Just run

```
npm run build
```

## Develop

Just run

```
npm run dev
```

And I recommend to keep the code consistent if your IDE doesn't support prettier

```
npm run prettier:watch
```

### Enjoy

License GPL-3.0
